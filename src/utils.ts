import { Subject } from 'rxjs';

export function randomInt(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

export const useScene = (space: string, name: string) => {
  return function (state: string) {
    return `${space}:${name}:${state}`;
  };
};

export function findDiff(str1: string, str2: string) {
  let diff = '';
  str2.split('').forEach(function (val, i) {
    if (val != str1.charAt(i)) diff += val;
  });
  return diff;
}

declare global {
  interface String {
    replaceVowels(): string;
  }
}

String.prototype.replaceVowels = function (this: string) {
  const mapping = { á: 'a', é: 'e', í: 'i', ó: 'o', ú: 'u' };
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  //@ts-ignore
  return this.replace(/(á)|(é)|(í)|(ó)|(ú)/gi, (k) => mapping[k]);
};

export function AutoDestroy(component: any, key: string | symbol): void {
  const originalOnDestroy = component.ngOnDestroy;

  component.ngOnDestroy = function () {
    const destroyed$ = this[key] as unknown;
    if (originalOnDestroy) {
      originalOnDestroy.call(this);
    }
    if (destroyed$ instanceof Subject) {
      destroyed$.next(undefined);
      destroyed$.complete();
    }
    // if (prop instanceof Subscription) {
    //   prop.unsubscribe();
    // }
  };
}
