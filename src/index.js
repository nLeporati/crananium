var express = require('express');

var app = express();

app.use(express.static('dist'));
app.get("/", function(_, res) {
  res.redirect('/');
});

app.listen(3000);
