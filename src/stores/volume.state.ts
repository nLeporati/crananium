import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class VolumeState {
  private readonly KEY = 'volume';

  public get value(): number {
    const value = localStorage.getItem(this.KEY);
    return value ? Number(value) : 50;
  }

  public set value(value: number) {
    localStorage.setItem(this.KEY, String(value));
  }
}
