import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';

function getMinutesBetweenDates(startDate: number, endDate: number) {
  return Math.floor((endDate - startDate) / 1000);
}

@Injectable({
  providedIn: 'root',
})
export class TimeState {
  private _state$ = new BehaviorSubject<number | undefined>(undefined);

  private get get(): number | undefined {
    const date = localStorage.getItem('start_time');
    return date ? Number(date) : undefined;
  }

  private set set(time: number) {
    const date = String(time);
    localStorage.setItem('start_time', date);
  }

  get timer() {
    return this._state$.getValue();
  }

  getDiff(timer: number): number {
    if (this.get === undefined) {
      return timer;
    } else {
      const diff = getMinutesBetweenDates(this.get, Date.now());
      return timer - diff < 0 ? 0 : timer - diff;
    }
  }

  start(timer: number) {
    if (!this.get) this.set = Date.now();
    this._state$.next(timer);
  }

  unset() {
    if (this.get) localStorage.removeItem('start_time');
    this._state$.next(undefined);
  }

  changes(): Observable<number | undefined> {
    return this._state$.asObservable();
  }
}
