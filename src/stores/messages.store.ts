import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as signalR from '@microsoft/signalr';
import { GameStore } from './game.store';
import { Message } from 'src/models/message';

@Injectable({
  providedIn: 'root',
})
export class MessagesStore {
  private _messages: BehaviorSubject<string[]>;
  private _conn?: signalR.HubConnection;

  constructor(private readonly _gameStore: GameStore) {
    this._messages = new BehaviorSubject<string[]>([]);
  }

  private async connect(): Promise<void> {
    if (this._conn) return;

    this._conn = new signalR.HubConnectionBuilder()
      .withUrl(`${environment.apiUrl}/messages`, { withCredentials: false })
      .withAutomaticReconnect()
      .build();

    this._conn.on('new-message', (message: Message) => {
      console.debug(`[connection][event] new-message`, message);
      this.addMessage(message);
    });

    this._conn.onreconnected(async (id) => {
      console.debug('[connection][reconnected] messages:', id);
      await this.watch(this._gameStore.room!.id);
    });

    this._conn.onclose(() => {
      console.debug('[connection][close] messages:');
    });

    try {
      await this._conn.start();
      console.info('[connection][start] messages');
      await this.watch(this._gameStore.room!.id);
    } catch (error) {
      console.error('[connection] error:', error);
    }
  }

  private async watch(roomId: string) {
    if (this._conn) {
      await this._conn.send('watch', roomId);
      console.debug('[connection][send] watch:', roomId);
    }
  }

  public async disconnect(): Promise<void> {
    this._messages.next([]);
    await this._conn?.stop();
    this._conn = undefined;
  }

  public async sendMessage(text: string, guess: boolean) {
    await this.connect();

    if (this._conn && this._gameStore.room) {
      const message: Message = {
        roomId: this._gameStore.room!.id,
        userId: this._gameStore.user!.id,
        guess,
        text,
      };
      this.addMessage(message);
      await this._conn.send('messages', message);
      console.debug('[connection][send] messages:', message);
    }
  }

  private addMessage(message: Message) {
    const user = this._gameStore.room?.users.find(
      (u) => u.id === message.userId
    );
    const msg = `(${user!.name}): ${message.text}`;
    this._messages.next([...this._messages.value, msg]);
  }

  public async getMessages() {
    await this.connect();
    return this._messages.asObservable();
  }
}
