import { UserState } from './user.state';
import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { GameConfig, GameData } from 'src/models/game.model';
import { Room, Scene } from 'src/models/room.model';
import { User, UserStatus } from 'src/models/user.model';
import { RoomService } from 'src/services/room.service';
import { CATEGORIES, GAMES } from 'src/environments/constants';
import * as signalR from '@microsoft/signalr';

export class GameState {
  private _room: Room;
  private _user: User;

  constructor(room: Room, user: User) {
    this._room = room;
    this._user = user;
  }

  public get areRoomOwner(): boolean {
    if (this._room.users.length === 0) return false;
    return this._room.users[0].id === this._user.id;
  }

  public get driver(): User {
    const id = this._room.game.driverId;
    return this._room.users.find((u) => u.id === id) ?? this._user;
  }

  public get isDriver(): boolean {
    return this.driver?.id === this._user.id;
  }

  public get driverName(): string {
    if (this.isDriver) return 'You';
    return this.driver.name;
  }

  public get winner(): User | undefined {
    const id = this._room.game.usersAnswered[0];
    return this._room.users.find((u) => u.id === id);
  }

  public get doYouWin(): boolean {
    return this.winner?.id === this._user.id;
  }

  public get winnerName(): string | undefined {
    if (this.doYouWin) return 'You';
    return this.winner?.name;
  }

  public get category(): string {
    const category = this._room.game.props.category;
    return CATEGORIES[category] ?? '';
  }

  public get name(): string | undefined {
    const name = this._room.game.props.name;
    return GAMES[name];
  }
}

@Injectable({
  providedIn: 'root',
})
export class GameStore {
  private _conn?: signalR.HubConnection;
  private readonly _room$ = new BehaviorSubject<Room | undefined>(undefined);
  private readonly _game$ = new BehaviorSubject<GameState | undefined>(
    undefined
  );
  private _dispose$?: signalR.ISubscription<Room>;

  constructor(
    private readonly _user: UserState,
    private readonly _roomService: RoomService
  ) {}

  get room(): Room | undefined {
    return this._room$.value;
  }

  private set room(room: Room | undefined) {
    if (room) {
      const user = room.users.find((u) => u.id === this.user?.id);
      if (user) {
        this.game = new GameState(room, user);
      }
    }

    this._room$.next(room);
  }

  get user(): User | undefined {
    return this._user.get();
  }

  private set user(user: User | undefined) {
    this._user.set(user);
  }

  get game(): GameState | undefined {
    return this._game$.value;
  }

  private set game(value: GameState | undefined) {
    this._game$.next(value);
  }

  // public get driver(): User {
  //   const id = this.room!.game!.driverId;
  //   return this.room!.users!.find((u) => u.id === id)!;
  // }

  // public get isDriver(): boolean {
  //   if (!this.room || !this.user) return false;
  //   return this.driver.id === this.user.id;
  // }

  // public get driverName(): string {
  //   if (this.isDriver) return 'You';
  //   return this.driver.name;
  // }

  // public get category(): string {
  //   const category = this.room?.game.props.category;
  //   if (category) return CATEGORIES[category];
  //   return '';
  // }

  // public get gameName(): string {
  //   const name = this.room?.game.props.name;
  //   if (name) return GAMES[name];
  //   return '';
  // }

  getRoom() {
    return this._room$.asObservable();
  }

  getUser() {
    return this._user.changes();
  }

  getGame(): Observable<GameState | undefined> {
    return this._game$.asObservable();
  }

  async createRoom(username: string, config: GameConfig): Promise<Room> {
    this.room = await this._roomService.createRoom(username, config);
    // this._gameStore.rounds = this.rounds; // TODO save config in localStorage
    this.user = this.room.users[0];
    return this.room;
  }

  async joinRoom(room: Room, username?: string): Promise<Room | undefined> {
    this.room = room;
    console.log('room', this.room);

    if (!username && this.room.scene === 'lobby') {
      username = this.user?.name ?? 'default';
      username = prompt('Please select a username', username) ?? username;
    }
    if (username) {
      if (this.user && this.user.name !== username) {
        this.user = { ...this.user, name: username, status: 'idle' };
        await this.updateUser(this.user);
      }
      const userExists = this.room.users.find((u) => u.id === this.user?.id);
      if (!userExists) {
        this.room = await this._roomService.joinRoom(username, this.room.id);
        this.user = this.room.users[this.room.users.length - 1];
      }
    }
    return this.room;
  }

  async findRoom(roomId: string): Promise<Room> {
    const room = await this._roomService.getRoom(roomId);
    return room;
  }

  async updateUser(user: User): Promise<void> {
    if (!this.room) return;
    this.room = await this._roomService.updateUser(this.room.id, user);
  }

  async updateStatus(status: UserStatus): Promise<void> {
    if (!this.user) return;
    if (status === this.user.status) return;
    const user: User = { ...this.user, status };
    await this.updateUser(user);
  }

  async watchRoom(room: Room): Promise<void> {
    this._conn = new signalR.HubConnectionBuilder()
      .withUrl(`${environment.apiUrl}/hub`, { withCredentials: false })
      .withAutomaticReconnect()
      .build();

    this._conn.on('room-changes', (room: Room) => {
      console.log(`[connection][event] room-changes`, room);
      this.room = room;
    });

    this._conn.on('error', (error: string) => {
      console.error(`[connection][event] error`, { message: error });
    });

    this._conn.onreconnected((id) => {
      console.log('[connection] reconnected:', id);
      this.subscribeOnRoom(room.id);
      if (this.room) this.findRoom(this.room.id);
    });

    try {
      await this._conn.start();
      await this.subscribeOnRoom(room.id);
    } catch (error) {
      console.error('[connection] error:', error);
    }
  }

  private async subscribeOnRoom(id: string) {
    if (this._conn) {
      await this._conn.send('room', id);
      // this._dispose$ = this._conn.stream<Room>('WatchRoom', id).subscribe({
      //   next: (room) => {
      //     console.log('room', room);
      //     this.room = room;
      //   },
      //   complete: () => console.log('complete'),
      //   error: (e) => console.log('error', e),
      // });
      console.log('[connection][send] room:', id);
    }
  }

  public async stopWatchRoom(): Promise<void> {
    this._dispose$?.dispose();
    await this._conn?.stop();
    this._conn = undefined;
  }

  public async updateRoomScene(scene: Scene): Promise<void> {
    if (!this.room) return;
    await this._roomService.updateRoomScene(this.room.id, scene);
  }

  async updateGameData<T extends GameData>(data: T): Promise<void> {
    if (!this.room) return;

    const oldData = this.room.game.data as T;
    const mergeData = { ...oldData, ...data } as T;

    await this._roomService.updateGameData(
      this.room.id,
      this.room.game.id,
      mergeData
    );
  }

  leaveRoom() {
    this.room = undefined;
    this.stopWatchRoom();
  }
}
