import { TestBed } from '@angular/core/testing';

import { PlayStore } from './play.store';

describe('PlayService', () => {
  let service: PlayStore;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlayStore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
