import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Game, GameData } from 'src/models/game.model';
import { PocketbaseService } from 'src/services/pocketbase.service';
import { GameStore } from './game.store';

@Injectable({
  providedIn: 'root'
})
export class PlayStore {
  private _game$ = new BehaviorSubject<Game<GameData> | undefined>(undefined);

  constructor(
    private readonly _gameStore: GameStore,
    private readonly _service: PocketbaseService
  ) { }

  get game() { return this._game$.value }
  set game(user: any) { this._game$.next(user) }

  getGame() { return this._game$.asObservable() }

  watchGame(gameId: string) {
    return this._service.room(gameId, (game) => {
      console.debug('new game data', game);
      this.game = game
    })
  }

  async createGame<T>(name: string, data: T): Promise<Game<T>> {
    const room = this._gameStore.room!
    return this._service.createGame(room.id, name, data)
  }

  async updateGameData<T>(data: T): Promise<Game<T>> {
    const room = this._gameStore.room!
    return this._service.updateGameData(room.id, room.game.id, data)
  }
}
