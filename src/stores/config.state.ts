import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { GameConfig } from 'src/models/game.model';
import { State } from 'src/models';

@Injectable({
  providedIn: 'root',
})
export class ConfigState implements State<GameConfig> {
  private readonly _key = 'config';
  private _state$ = new BehaviorSubject<GameConfig | undefined>(undefined);

  get() {
    const value = localStorage.getItem(this._key);
    return value ? (JSON.parse(value) as GameConfig) : undefined;
  }

  set(value: GameConfig | undefined) {
    if (value === undefined) localStorage.removeItem(this._key);
    else localStorage.setItem(this._key, JSON.stringify(value));

    this._state$.next(value);
  }

  get changes() {
    return this._state$.asObservable();
  }
}
