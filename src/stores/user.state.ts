import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserState {
  private _state$ = new BehaviorSubject<User | undefined>(this.getUser());

  private getUser(): User | undefined {
    const user = localStorage.getItem('user') ?? undefined;
    if (user != undefined) {
      return JSON.parse(user) as User;
    }
    return user;
  }

  get(): User | undefined {
    return this._state$.value;
  }
  set(value: User | undefined) {
    if (value !== undefined) {
      localStorage.setItem('user', JSON.stringify(value));
    }
    this._state$.next(value);
  }
  unset() {
    localStorage.removeItem('user');
  }
  changes(): Observable<User | undefined> {
    return this._state$.asObservable();
  }
}
