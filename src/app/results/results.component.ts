import { Component, OnInit } from '@angular/core';
import { filter, ReplaySubject, takeUntil, tap } from 'rxjs';
import { Game, Song, SongGuessData } from 'src/models/game.model';
import { Room } from 'src/models/room.model';
import { GameStore } from 'src/stores/game.store';
import { AutoDestroy } from 'src/utils';

interface Results {
  id: string | null;
  winerName: string;
  round: number;
  song: Song;
}

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
})
export class ResultsComponent implements OnInit {
  public results: Results[] = [];
  public rank: { id: string | null; name: string; wins: number }[] = [];

  @AutoDestroy private destroy$ = new ReplaySubject<void>(1);

  constructor(private readonly _gameStore: GameStore) {}

  ngOnInit(): void {
    this._gameStore
      .getRoom()
      .pipe(
        filter((room) => !!room),
        tap((room) => this.mapResults(room!)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  get rounds() {
    return this._gameStore.room?.config.rounds;
  }

  mapResults(room: Room) {
    this.rank = [];
    const results = room.results as Game<SongGuessData>[];
    this.results = results.map((result, i) => {
      const user = room?.users.filter(
        (u) => u.id == result.usersAnswered[0]
      )[0];
      return {
        round: i,
        song: result.data.song!,
        id: user ? user.id : null,
        winerName: user ? user.name : 'nobody',
      };
    });
    this.results
      .filter((r) => r.id)
      .forEach((r) => {
        const wins = this.results.filter(
          (f) => f.winerName == r.winerName
        ).length;
        if (this.rank.filter((u) => u.name == r.winerName).length == 0) {
          this.rank.push({
            id: r.id,
            name: r.winerName,
            wins,
          });
        }
      });
    this.rank.sort((a, b) => b.wins - a.wins);
  }
}
