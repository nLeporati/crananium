import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable, tap, ReplaySubject, takeUntil } from 'rxjs';
import { Room } from 'src/models/room.model';
import { User } from 'src/models/user.model';
import { GameStore } from 'src/stores/game.store';
import { AutoDestroy } from 'src/utils';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss'],
})
export class LobbyComponent implements OnInit {
  ready: boolean;
  room$?: Observable<Room | undefined>;
  user$?: Observable<User | undefined>;
  isLoading: boolean;

  @AutoDestroy private _destroy$ = new ReplaySubject<void>(1);

  constructor(
    private readonly _router: Router,
    private readonly _gameStore: GameStore
  ) {
    this.isLoading = false;
    this.ready = this.checkStatus(this._gameStore.user);
  }

  ngOnInit(): void {
    this.room$ = this._gameStore.getRoom();
    this.user$ = this._gameStore.getUser();
    this._gameStore
      .getUser()
      .pipe(
        tap((user) => this.checkStatus(user)),
        takeUntil(this._destroy$)
      )
      .subscribe();
  }

  checkStatus(user?: User): boolean {
    this.ready = user?.status === 'ready';
    return this.ready;
  }

  async onChangeStatus() {
    this.isLoading = true;
    this.ready = !this.ready;
    await this._gameStore.updateStatus(this.ready ? 'ready' : 'idle');
    this.isLoading = false;
  }

  onLeave() {
    this._router.navigate(['']);
  }
}
