import { Component, OnInit } from '@angular/core';
import {
  BehaviorSubject,
  filter,
  Observable,
  ReplaySubject,
  take,
  takeUntil,
  tap,
} from 'rxjs';
import { TriviaData } from 'src/models/game.model';
import { Room, Scene, SceneEnum } from 'src/models/room.model';
import { GameState, GameStore } from 'src/stores/game.store';
import { MessagesStore } from 'src/stores/messages.store';
import { TimeState } from 'src/stores/time.state';
import { AutoDestroy } from 'src/utils';
import { Question } from './models/question';
import { TriviaService } from './services/trivia.service';

@Component({
  selector: 'app-game-trivia',
  templateUrl: './trivia.component.html',
  styleUrls: ['./trivia.component.scss'],
})
export class TriviaComponent implements OnInit {
  scene: Scene;
  loading: boolean;
  youGuess?: boolean;

  // room?: Room;
  question?: Question;
  selectedAnswer?: string;

  room$: Observable<Room | undefined>;
  game$: Observable<GameState | undefined>;

  @AutoDestroy q = new BehaviorSubject<boolean>(false);
  @AutoDestroy private _destroy$ = new ReplaySubject(1);

  constructor(
    private readonly _timeState: TimeState,
    private readonly _gameStore: GameStore,
    private readonly _messagesStore: MessagesStore,
    private readonly _api: TriviaService
  ) {
    this.loading = true;
    this.scene = SceneEnum.Home;
    this.room$ = this._gameStore.getRoom();
    this.game$ = this._gameStore.getGame();
  }

  ngOnInit(): void {
    this.q
      .asObservable()
      .pipe(
        filter((hasSearch) => hasSearch),
        take(1),
        tap(() => {
          console.log('enter');
          this.getQuestion();
        })
      )
      .subscribe();
    this._gameStore
      .getRoom()
      .pipe(
        tap((room) => this.handleRoomChanges(room)),
        takeUntil(this._destroy$)
      )
      .subscribe();
  }

  async handleRoomChanges(room?: Room) {
    // this.room = room;
    if (!room) return;
    const scene = room.scene;
    // this.isDriver = this._gameStore.isDriver;
    // this.driverName = this._gameStore.driverName;
    // this.category = this._gameStore.category;

    if (this.loading && this.scene != scene) {
      this.loading = false;
    }

    this.scene = scene;
    const data = room.game.data as TriviaData;

    switch (scene) {
      case SceneEnum.GameStart:
        if (data.question !== null) {
          this._timeState.unset();
          this.question = data.question;
          this._gameStore.updateStatus('playing');
        }
        if (this._gameStore.game?.isDriver === true) {
          this.q.next(true);
        }
        break;
      case SceneEnum.GameRun:
        if (data.question && !this.question) {
          this.question = data.question;
        }
        break;
      case SceneEnum.GameEnd:
        if (data.question && !this.question) {
          this.question = data.question;
        }
        // this.resultSong = data.song;
        break;
      default:
        break;
    }
  }

  async getQuestion() {
    const question = await this._api.getQuestions();
    this.question = question;
    await this._gameStore.updateGameData({ question });
  }

  async onReady() {
    await this._gameStore.updateRoomScene(SceneEnum.GameStart);
  }

  async onGuess(event: { guess: boolean; answer?: string }) {
    const { guess, answer } = event;

    const message = guess
      ? 'Guess the question'
      : 'Gave a bad answer... lost :(';
    await this._messagesStore.sendMessage(message, guess);

    this.youGuess = guess;
    if (answer) this.selectedAnswer = answer;

    if (answer === undefined) {
      // TODO have to wait to all answers or time end
      await this._gameStore.updateRoomScene(SceneEnum.GameEnd);
    }
  }

  onNext() {
    this._gameStore.updateRoomScene(SceneEnum.Next);
  }
}
