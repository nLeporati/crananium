import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { Question } from '../models/question';

@Injectable()
export class TriviaService {
  private _url = 'https://the-trivia-api.com/api';
  private _questions: Set<Question> = new Set();

  private url(uri: string, query = ''): string {
    return `${this._url}${uri}${query}`;
  }

  constructor(private _http: HttpClient) {}

  // async getCategories(id?: string): Promise<Question[]> {
  //   const uri = id ? `/questions/${id}` : '/genres';
  //   const data = await lastValueFrom(
  //     this._http.get<NapsterGenres>(this.url(uri))
  //   );
  //   return data;
  // }

  async getQuestions(limit?: number): Promise<Question> {
    const data = this._http.get<Question[]>(
      this.url(`/questions`, `?limit=${limit ?? 1}`)
    );
    return (await lastValueFrom(data))[0];
  }

  // selectSong(tracks: Track[]): Song {
  //   const index = randomInt(0, tracks.length - 1);
  //   const track = tracks[index];
  //   const song: Song = {
  //     name: track.name,
  //     image: `https://api.napster.com/imageserver/v2/albums/${track.albumId}/images/300x300.jpg`,
  //     audio: track.previewURL,
  //     artist: track.artistName,
  //   };
  //   this._questions.add(song);
  //   return song;
  // }
}
