import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TriviaComponent } from './trivia.component';
import { TriviaService } from './services/trivia.service';
import { TimerComponent } from 'src/lib/timer/timer.component';
import { GameInfoComponent } from 'src/app/game-info/game-info.component';
import { AvatarComponent } from 'src/lib/avatar/avatar.component';
import { MessagesComponent } from 'src/lib/messages/messages.component';
import { GameResultComponent } from 'src/lib/game-result/game-result.component';
import { QuestionComponent } from './question/question.component';

@NgModule({
  declarations: [TriviaComponent, QuestionComponent],
  providers: [TriviaService],
  imports: [
    CommonModule,
    TimerComponent,
    MessagesComponent,
    GameInfoComponent,
    AvatarComponent,
    GameResultComponent,
  ],
  exports: [TriviaComponent],
})
export class TriviaModule {}
