import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from 'src/models/user.model';
import { Question } from '../models/question';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss'],
})
export class QuestionComponent {
  options?: { answer: string; correct: boolean }[];
  @Input() guessing = false;

  @Input() answer?: string;
  @Input() doYouWin?: boolean;
  @Input() winner?: User;
  @Input() question!: Question;

  @Output() guess = new EventEmitter<{ guess: boolean; answer?: string }>();

  onGuess(guess: boolean, answer?: string) {
    this.doYouWin = guess;
    this.answer = answer;

    this.guess.emit({ guess, answer });
  }
}
