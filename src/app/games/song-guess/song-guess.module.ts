import { AvatarComponent } from '../../../lib/avatar/avatar.component';
import { StarIconComponent } from '../../../lib/star-icon/star-icon.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { SongPlayerComponent } from './song-player/song-player.component';
import { SongResultComponent } from './song-result/song-result.component';
import { SongGuessService } from './services/song-guess.service';
import { SongGuessComponent } from './song-guess.component';
import { SongSelectorComponent } from './song-selector/song-selector.component';
import { TimerComponent } from 'src/lib/timer/timer.component';
import { IconsModule } from 'src/lib/icons/icons.module';
import { GameInfoComponent } from 'src/app/game-info/game-info.component';
import { MessagesComponent } from 'src/lib/messages/messages.component';

@NgModule({
  declarations: [
    SongGuessComponent,
    SongSelectorComponent,
    SongResultComponent,
    SongPlayerComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,

    IconsModule,
    StarIconComponent,
    GameInfoComponent,
    AvatarComponent,
    TimerComponent,
    MessagesComponent,
  ],
  providers: [SongGuessService],
  exports: [SongGuessComponent],
})
export class SongGuessModule {}
