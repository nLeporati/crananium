import { Component, OnInit } from '@angular/core';
import { ReplaySubject, takeUntil, tap } from 'rxjs';
import { Song, SongGuessData } from 'src/models/game.model';
import { Room, Scene, SceneEnum } from 'src/models/room.model';
import { GameStore } from 'src/stores/game.store';
import { TimeState } from 'src/stores/time.state';
import { AutoDestroy } from 'src/utils';

@Component({
  selector: 'app-song-guess',
  templateUrl: './song-guess.component.html',
  styleUrls: ['./song-guess.component.scss'],
})
export class SongGuessComponent implements OnInit {
  loading: boolean;
  scene: Scene;
  isDriver: boolean;
  category: string;

  winnerId: string;
  song?: Song;
  resultSong?: Song;
  driverName?: string;

  room?: Room;

  @AutoDestroy private _destroy$ = new ReplaySubject(1);

  constructor(
    private readonly _gameStore: GameStore,
    private readonly _timeState: TimeState
  ) {
    this.scene = SceneEnum.Home;
    this.winnerId = '';
    this.category = '';
    this.isDriver = this._gameStore.game!.isDriver;
    this.loading = true;
  }

  ngOnInit(): void {
    this._gameStore
      .getRoom()
      .pipe(
        tap((room) => this.handleRoomChanges(room)),
        takeUntil(this._destroy$)
      )
      .subscribe();
  }

  handleRoomChanges(room?: Room) {
    this.room = room;
    if (!room) return;
    const scene = room.scene;
    this.isDriver = this._gameStore.game!.isDriver;
    this.driverName = this._gameStore.game!.driverName;
    this.category = this._gameStore.game!.category;

    if (this.loading && this.scene != scene) {
      this.loading = false;
    }

    this.scene = scene;
    const data = room.game.data as SongGuessData;

    switch (this.scene) {
      case SceneEnum.GameStart:
        this._timeState.unset();
        this.resultSong = undefined;
        this.song = undefined;
        break;
      case SceneEnum.GameRun:
        if (data.song && !this.song) {
          this.song = data.song;
        }
        break;
      case SceneEnum.GameEnd:
        this.song = undefined;
        this.resultSong = data.song;
        this.winnerId = room.game.usersAnswered[0];
        break;
      default:
        break;
    }
  }

  async onReady() {
    this.loading = true;
    await this._gameStore.updateRoomScene(SceneEnum.GameStart);
  }

  async onChooseSong(song: Song) {
    this.loading = true;
    await this._gameStore.updateGameData({ song });
  }

  async onGuessSong(guess: boolean) {
    if (guess) {
      // this.song = undefined;
      this.resultSong = this.song;
      // this.winnerId = this._gameStore.user!.id;
    } else {
      this._gameStore.updateRoomScene(SceneEnum.GameEnd);
    }
  }

  async onNext() {
    this.loading = true;
    this.resultSong = undefined;
    this.song = undefined;
    if (
      this._gameStore.room!.results.length + 1 >=
      this._gameStore.room!.config.rounds
    ) {
      this._gameStore.updateRoomScene(SceneEnum.Results);
    } else {
      this._gameStore.updateRoomScene(SceneEnum.Next);
    }
  }
}
