import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SongGuessComponent } from './song-guess.component';

describe('SongGuessComponent', () => {
  let component: SongGuessComponent;
  let fixture: ComponentFixture<SongGuessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SongGuessComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SongGuessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
