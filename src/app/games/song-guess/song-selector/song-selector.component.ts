import { Genre } from './../models/napster-api.models';
import { Song } from 'src/models/game.model';
import { SongGuessService } from './../services/song-guess.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

const RANDOM_GENRES = 'g.115,g.5,g.209,g.359,g.515';

@Component({
  selector: 'app-song-selector',
  templateUrl: './song-selector.component.html',
  styleUrls: ['./song-selector.component.scss'],
})
export class SongSelectorComponent implements OnInit {
  genres: Genre[] = [];
  genresCache: Genre[] = [];
  isLoading = true;

  allGenre?: Genre;
  selectedGenre?: Genre;
  selectedSubGenre?: Genre;
  song?: Song;

  @Input() variant!: string;
  @Output() chooseSong = new EventEmitter<Song>();

  constructor(private readonly _service: SongGuessService) {}

  ngOnInit(): void {
    if (this.variant === 'chosen') this.getGenres();
    else {
      this.getSong(RANDOM_GENRES, 14);
    }
  }

  getGenres(id?: string): void {
    this.isLoading = true;
    this._service.getGenres(id).then((data) => {
      if (this.genres.length === 0) {
        // make a cache only the first time for the genres
        this.genresCache = data.genres;
      }
      this.genres = data.genres;
      this.isLoading = false;
    });
  }

  async selectGenre(genre: Genre): Promise<void> {
    if (this.selectedGenre) {
      this.getSong(genre.id);
      return;
    }

    this.selectedGenre = genre;
    const ids = this.selectedGenre.links.childGenres.ids.reduce(
      (p, c) => p + ',' + c
    );
    this.allGenre = this.selectedGenre;
    this.getGenres(ids);
  }

  async getSong(id: string, limit?: number): Promise<void> {
    this.isLoading = true;
    const tracks = (await this._service.getSongs(id, limit)).tracks;

    if (tracks.length > 0) {
      this.song = this._service.selectSong(tracks);
      this.isLoading = false;
      this.genres = [];
      this.chooseSong.emit(this.song);
    } else {
      if (!this.selectedGenre) return;
      const ids = (this.selectedGenre.links.childGenres.ids as string[]).reduce(
        (p, c) => p + ',' + c
      );
      this.getSong(ids);
      // TODO handle infinite loop
    }
  }

  onReturn() {
    this.allGenre = undefined;
    this.selectedGenre = undefined;
    this.genres = this.genresCache;
  }
}
