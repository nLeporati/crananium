import {
  Component,
  EventEmitter,
  Input,
  Output,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { Observable } from 'rxjs';
import { Song } from 'src/models/game.model';
import { User } from 'src/models/user.model';
import { GameStore } from 'src/stores/game.store';

@Component({
  selector: 'app-song-result',
  templateUrl: './song-result.component.html',
})
export class SongResultComponent implements OnChanges {
  doYouWin: boolean;
  winnerName: string;
  driverName: string;

  @Input() song?: Song;
  @Input() winnerId!: string;
  @Input() enabledNext!: boolean;
  @Output() next = new EventEmitter();

  constructor(private readonly _gameStore: GameStore) {
    this.doYouWin = this.getDoYouWin();
    this.winnerName = this.getWinnerName();
    this.driverName = this.getDriverName();
  }

  ngOnChanges(_: SimpleChanges): void {
    this.doYouWin = this.getDoYouWin();
    this.winnerName = this.getWinnerName();
    this.driverName = this.getDriverName();
  }

  getDoYouWin(): boolean {
    return this.winnerId === this._gameStore.user?.id;
  }

  getWinnerName(): string {
    if (this.doYouWin) return 'you';
    return (
      this._gameStore.room?.users.find((u) => u.id === this.winnerId)?.name ??
      ''
    );
  }

  getDriverName(): string {
    return this._gameStore.game!.driverName;
  }

  onNext() {
    this.next.emit();
  }
}
