import { environment } from './../../../../environments/environment';
import {
  NapsterGenres,
  NapsterTracks,
  Track,
} from './../models/napster-api.models';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { randomInt } from 'src/utils';
import { Song } from 'src/models/game.model';

const API_KEY = environment.napsterApiKey; //process.env.NG_APP_NAPSTER_API_KEY;

@Injectable()
export class SongGuessService {
  private _url = 'https://api.napster.com/v2.2';
  private _apiKey = `?apikey=${API_KEY}`;
  private _songs: Set<Song> = new Set();

  private url(uri: string, query = ''): string {
    return `${this._url}${uri}${this._apiKey}${query}`;
  }

  constructor(private _http: HttpClient) {}

  async getGenres(id?: string): Promise<NapsterGenres> {
    const uri = id ? `/genres/${id}` : '/genres';
    const data = await lastValueFrom(
      this._http.get<NapsterGenres>(this.url(uri))
    );
    return data;
  }

  async getSongs(id: string, limit?: number): Promise<NapsterTracks> {
    const data = this._http.get<NapsterTracks>(
      this.url(`/genres/${id}/tracks/top`, `&limit=${limit ?? 20}`)
    );
    return await lastValueFrom(data);
  }

  selectSong(tracks: Track[]): Song {
    const index = randomInt(0, tracks.length - 1);
    const track = tracks[index];
    const song: Song = {
      name: track.name,
      image: `https://api.napster.com/imageserver/v2/albums/${track.albumId}/images/300x300.jpg`,
      audio: track.previewURL,
      artist: track.artistName,
    };
    this._songs.add(song);
    return song;
  }
}
