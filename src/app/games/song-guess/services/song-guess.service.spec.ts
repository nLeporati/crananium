import { TestBed } from '@angular/core/testing';

import { SongGuessService } from './song-guess.service';

describe('SongGuessService', () => {
  let service: SongGuessService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SongGuessService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
