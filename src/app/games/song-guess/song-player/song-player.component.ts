import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  OnDestroy,
} from '@angular/core';
import { tap, takeUntil, filter, ReplaySubject, Observable, take } from 'rxjs';
import { Song } from 'src/models/game.model';
import { User } from 'src/models/user.model';
import { GameStore } from 'src/stores/game.store';
import { MessagesStore } from 'src/stores/messages.store';
import { TimeState } from 'src/stores/time.state';
import { VolumeState } from 'src/stores/volume.state';
import { AutoDestroy } from 'src/utils';

@Component({
  selector: 'app-song-player',
  templateUrl: './song-player.component.html',
  styleUrls: ['./song-player.component.scss'],
})
export class SongPlayerComponent implements OnInit, OnDestroy {
  win: boolean;
  guess: string;
  volume: number;
  loading: boolean;
  audio: HTMLAudioElement;

  timer$: Observable<number | undefined>;
  messages$?: Observable<string[]>;

  @Input() song!: Song;
  @Input() user?: User;
  @Output() guessSong = new EventEmitter<boolean>();

  @AutoDestroy private _destroy$ = new ReplaySubject<void>(1);

  constructor(
    private readonly _gameStore: GameStore,
    private readonly _timeState: TimeState,
    private readonly _volumeState: VolumeState,
    private readonly _messagesStore: MessagesStore
  ) {
    this.win = false;
    this.guess = '';
    this.loading = true;
    this.audio = new Audio();
    this.volume = this._volumeState.value;
    this.setVolume(this.volume);

    this.timer$ = this._timeState.changes();
  }

  ngOnInit(): void {
    console.log(`song:%c${this.song.name}`, 'color: #fff');

    this.watchMessages();

    this._timeState
      .changes()
      .pipe(
        filter((timer) => !!timer),
        take(1),
        tap(() => this.playMusic()),
        takeUntil(this._destroy$)
      )
      .subscribe();

    this.audio.src = this.song.audio;
    this.audio.addEventListener('canplaythrough', () => this.onReady());
    this.audio.load();
  }

  ngOnDestroy(): void {
    this.removeAudio();
    this._messagesStore.disconnect();
  }

  async watchMessages() {
    this.messages$ = await this._messagesStore.getMessages();
  }

  async onReady() {
    if (this._timeState.timer === undefined)
      await this._gameStore.updateStatus('playing');
    else this.playMusic();
  }

  playMusic() {
    // if (this.audio.src && this.audio.paused) // TODO check functionality
    this.audio.play();
    this.loading = false;
  }

  setVolume(value: number) {
    this.audio.volume = value / 100;
    this._volumeState.value = value;
  }

  async onSubmit() {
    if (this.guess === '') return;
    const guess = this.guess
      .toLowerCase()
      .replace(/’`/g, "'")
      .replaceVowels()
      .trim();
    const songName = this.song.name
      .toLowerCase()
      .replace(/ \([^]*?\)/g, '')
      .replace(/ \[[^]*?\]/g, '')
      .replace(/’`/g, "'")
      .replaceVowels();
    const cleanSongName = songName.replace(/[^a-z ]/g, '').trim();

    this.win = guess === songName || guess === cleanSongName;

    this._messagesStore.sendMessage(guess, this.win);

    if (this.win) {
      this.removeAudio();
      this.guessSong.emit(this.win);
    } else {
      this.guess = '';
    }
  }

  removeAudio() {
    this.audio.pause();
    this.audio.src = '';
    this.audio = new Audio();
  }
}
