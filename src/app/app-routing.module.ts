import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameComponent } from './game/game.component';
import { HomeComponent } from './home/home.component';
import { LobbyComponent } from './lobby/lobby.component';
import { ResultsComponent } from './results/results.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'results', component: ResultsComponent },
  { path: 'r/:roomId', component: GameComponent },
  // {
  //   path: 'r/:roomId/g',
  //   loadChildren: () =>
  //     import('./games/games.module').then((m) => m.GamesModule),
  // },
  // {
  //   path: 'r/:roomId/g',
  //   component: GameComponent,
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
