import { AvatarComponent } from '../lib/avatar/avatar.component';
import { SongGuessModule } from './games/song-guess/song-guess.module';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { LobbyComponent } from './lobby/lobby.component';
import { ShellComponent } from './shell/shell.component';
import { ResultsComponent } from './results/results.component';
import { HttpClientModule } from '@angular/common/http';
import { GameComponent } from './game/game.component';
import { TriviaModule } from './games/trivia/trivia.module';

@NgModule({
  declarations: [
    ShellComponent,
    HomeComponent,
    LobbyComponent,
    ResultsComponent,
    GameComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,

    AvatarComponent,
    SongGuessModule,
    TriviaModule,
  ],
  providers: [],
  bootstrap: [ShellComponent],
})
export class AppModule {}
