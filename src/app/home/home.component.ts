import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DEFAULT_CONFIG, GAME_LIST } from 'src/environments/constants';
import { Dropdown } from 'src/models';
import { GameConfig } from 'src/models/game.model';
import { ConfigState } from 'src/stores/config.state';
import { GameStore } from 'src/stores/game.store';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  changeDriver: boolean;
  rounds: number;
  games: Dropdown;
  username: string;
  roomId: string;

  loading = false;
  showSettings = false;

  constructor(
    private readonly _configState: ConfigState,
    private readonly _gameStore: GameStore,
    private readonly _router: Router
  ) {
    const config = this._configState.get() ?? DEFAULT_CONFIG;
    this.games = new Dropdown(GAME_LIST, config.games);
    this.changeDriver = config.changeDriver;
    this.rounds = config.rounds;
    this.username = '';
    this.roomId = '';
  }

  ngOnInit(): void {
    this._gameStore.leaveRoom();
    this._gameStore
      .getUser()
      .subscribe((user) => (this.username = user?.name ?? ''));
    this._gameStore
      .getRoom()
      .subscribe((room) => (this.roomId = room?.id ?? ''));
  }

  async onNewGame() {
    const config: GameConfig = {
      changeDriver: this.changeDriver,
      rounds: this.rounds,
      games: this.games.selected,
    };

    this.loading = true;
    this._configState.set(config);
    const room = await this._gameStore.createRoom(this.username, config);
    this._router.navigate(['r', room.id]);
  }

  async onJoinGame() {
    this.loading = true;

    const room = await this._gameStore.findRoom(this.roomId);
    if (room) {
      await this._gameStore.joinRoom(room, this.username);
      this._router.navigate(['r', room.id]);
    } else {
      alert('Room not found');
    }

    this.loading = false;
  }
}
