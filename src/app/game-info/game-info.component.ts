import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { AvatarComponent } from 'src/lib/avatar/avatar.component';
import { Game, GameData } from 'src/models/game.model';
import { GameState, GameStore } from 'src/stores/game.store';

@Component({
  selector: 'app-game-info',
  standalone: true,
  templateUrl: './game-info.component.html',
  styleUrls: ['./game-info.component.scss'],
  imports: [CommonModule, AvatarComponent],
})
export class GameInfoComponent {
  @Input() game!: Game<GameData>;
  @Output() ready = new EventEmitter<void>();

  game$: Observable<GameState | undefined>;

  constructor(private readonly _gameStore: GameStore) {
    this.game$ = this._gameStore.getGame();
  }
}
