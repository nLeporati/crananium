import { Location } from '@angular/common';
import { Observable, tap } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Room } from 'src/models/room.model';
import { GameStore } from 'src/stores/game.store';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit, OnDestroy {
  room$?: Observable<Room | undefined>;

  constructor(
    private readonly _location: Location,
    private readonly _route: ActivatedRoute,
    private readonly _gameStore: GameStore
  ) {}

  ngOnInit(): void {
    this._route.params
      .pipe(
        tap((params) => {
          const roomId = params['roomId'] as string;
          if (roomId) this.joinRoom(params['roomId'] as string);
        })
      )
      .subscribe();

    this.room$ = this._gameStore.getRoom();
  }

  ngOnDestroy(): void {
    this._gameStore.stopWatchRoom();
  }

  checkRoom(room: Room | string | null) {
    if (room) return;
    alert('Room not found');
    this._location.go('.');
  }

  async joinRoom(roomId: string) {
    const room = await this._gameStore.findRoom(roomId);
    // console.log('room found', this._gameStore.room);

    // let userExists = false;
    // if (this._gameStore.user) {
    //   userExists = this._gameStore.room!.users.some(
    //     (u) => u.id === this._gameStore.user!.id
    //   );
    // }

    // if (!userExists && this._gameStore.room?.scene == 'lobby') {
    //   const username = this._gameStore.user?.name ?? 'default';
    //   const newName = prompt('Please select a username', username) ?? username;
    //   this._gameStore.joinRoom(newName, roomId);
    // }
    // if (this._gameStore.room?.scene == 'lobby') {
    // let username = this._gameStore.user?.name ?? 'default';
    // username = prompt('Please select a username', username) ?? username;
    if (!room) return;
    await this._gameStore.watchRoom(room);

    if (!this._gameStore.room) await this._gameStore.joinRoom(room);
    // }
  }
}
