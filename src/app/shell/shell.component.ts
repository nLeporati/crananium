import { Observable } from 'rxjs';
import { GameStore } from 'src/stores/game.store';
import { Room } from 'src/models/room.model';
import { User } from 'src/models/user.model';
import { Component } from '@angular/core';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
})
export class ShellComponent {
  user$: Observable<User | undefined>;
  room$: Observable<Room | undefined>;
  showDetails: boolean;

  constructor(private readonly _gameStore: GameStore) {
    this.user$ = this._gameStore.getUser();
    this.room$ = this._gameStore.getRoom();
    this.showDetails = false;
  }

  getPlayersNames(users: User[]): string {
    return users.reduce((p, c) => p + ' - ' + c.name, '') ?? '';
  }

  onClickMenu() {
    this.showDetails = !this.showDetails;
  }

  copyLink() {
    navigator.clipboard.writeText(window.location.href);
  }
}
