export interface Message {
  roomId: string;
  userId: string;
  guess: boolean;
  text: string;
}
