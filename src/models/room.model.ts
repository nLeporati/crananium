import { Game, GameConfig, GameData } from './game.model';
import { User, UserStatus } from './user.model';

export interface Room {
  id: string;
  users: User[];
  game: Game<GameData>;
  config: GameConfig;
  results: Game<GameData>[];
  scene: Scene;
  allUserStatus?: UserStatus;
  round: number;
}

export type Scene =
  | 'home'
  | 'lobby'
  | 'next'
  | 'game:info'
  | 'game:start'
  | 'game:run'
  | 'game:end'
  | 'results';

type SceneMap = {
  Home: Scene;
  Lobby: Scene;
  Next: Scene;
  GameInfo: Scene;
  GameStart: Scene;
  GameRun: Scene;
  GameEnd: Scene;
  Results: Scene;
};

const scenes: () => SceneMap = () => {
  return {
    Home: 'home',
    Lobby: 'lobby',
    Next: 'next',
    GameInfo: 'game:info',
    GameStart: 'game:start',
    GameRun: 'game:run',
    GameEnd: 'game:end',
    Results: 'results',
  };
};

export const SceneEnum = scenes();
