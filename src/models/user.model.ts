export type UserStatus = 'idle' | 'ready' | 'waiting' | 'playing';

export interface User {
  id: string;
  name: string;
  status: UserStatus;
}
