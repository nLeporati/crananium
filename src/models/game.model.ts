import { Question } from 'src/app/games/trivia/models/question';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface GameData {}

export class GameConfig {
  rounds = 5;
  changeDriver = true;
  games: string[] = [];
}

export interface GameProps {
  name: string;
  variant: string;
  style: string;
  category: string;
}

export interface Game<GameData> {
  id: string;
  props: GameProps;
  driverId: string;
  mode: string;
  data: GameData;
  usersAnswered: string[];
}

export class SongGuessData implements GameData {
  song?: Song;
}

export class TriviaData implements GameData {
  question?: Question;
}

export type Song = {
  name: string;
  audio: string;
  image: string;
  artist: string;
};
