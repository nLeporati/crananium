import { Observable } from 'rxjs';

export interface State<T> {
  get: () => T | undefined;
  set: (value: T | undefined) => void;
  changes: Observable<T | undefined>;
}

export type KeyMap<T> = {
  [key: string]: T;
};

export class Dropdown {
  public options: KeyMap<boolean>;

  constructor(options: KeyMap<boolean>, list?: string[]) {
    this.options = options;

    if (list) {
      Object.keys(options).forEach((key) => {
        const enabled = list.some((item) => item === key);
        this.options[key] = enabled;
      });
    }
  }

  public get selected(): string[] {
    return Object.entries(this.options)
      .filter((g) => g[1] === true)
      .map((g) => g[0]);
  }
}
