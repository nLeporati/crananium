// import Hashids from 'hashids';
// import { Room } from 'src/models/room.model';
// import { Injectable } from '@angular/core';
// import { User } from 'src/models/user.model';

// interface Users {
//   [key: string]: User;
// }

// export interface Rooms {
//   [key: string]: Room;
// }

// @Injectable({
//   providedIn: 'root',
// })
// export class LocalService {
//   user: User;
//   private _hashids: any;
//   private _users: Users = {};
//   private _rooms: Rooms = {};

//   constructor() {
//     this._hashids = new Hashids('', 5);
//     const userId = this._hashids.encode(Object.keys(this._users).length);
//     this.user = { id: userId, name: '', status: 'idle' };
//   }

//   createRoomId(username: string): string {
//     this.user.name = username;
//     const roomId = this._hashids.encode(Object.keys(this._rooms).length);
//     this._rooms[roomId] = {
//       id: roomId,
//       users: [this.user],
//       allUserStatus: 'idle',
//       scene: 'lobby',
//       game: {
//         id: '1',
//         data: { },
//         name: 'songguess',
//         results: []
//       }
//     };

//     return roomId;
//   }

//   createRoom(username: string): Room {
//     this.user.name = username;
//     const roomId = this._hashids.encode(Object.keys(this._rooms).length);
//     this._rooms[roomId] = {
//       id: roomId,
//       users: [this.user],
//       allUserStatus: 'idle',
//       scene: 'lobby',
//       game: {
//         id: '1',
//         data: { },
//         name: 'songguess',
//         results: []
//       }
//     };
//     console.log(this._rooms);

//     return this._rooms[roomId];
//   }

//   getRoom(id: string): Room | null {
//     return this._rooms[id];
//   }

//   setReady(roomId: string) {
//     this._rooms[roomId].allUserStatus = 'ready';
//   }

//   joinRoom(username: string, roomId: string) {
//     const id = '1';
//     // this._users[id] = { id, name: username, status: 'waiting' }
//   }
// }
