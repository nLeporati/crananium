import { environment } from './../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { Room } from 'src/models/room.model';
import { User } from 'src/models/user.model';
import { GameConfig } from 'src/models/game.model';

const _url = environment.apiUrl + '/rooms/'; //process.env.NG_APP_API_URL;
const url = (uri: string) => _url + uri;

@Injectable({
  providedIn: 'root',
})
export class RoomService {
  constructor(private _http: HttpClient) {}

  public createRoom(username: string, config: GameConfig): Promise<Room> {
    const rsp = this._http.post<Room>(url(`?username=${username}`), config);
    return firstValueFrom(rsp);
  }

  public async joinRoom(username: string, roomId: string): Promise<Room> {
    const rsp = this._http.post<Room>(url(`${roomId}/users/${username}`), {});
    return firstValueFrom(rsp);
  }

  public async getRoom(roomId: string): Promise<Room> {
    const rsp = this._http.get<Room>(url(`${roomId}`));
    return firstValueFrom(rsp);
  }

  public async updateUser(roomId: string, user: User): Promise<Room> {
    const rsp = this._http.put<Room>(url(`${roomId}/users`), user);
    return firstValueFrom(rsp);
  }

  public async updateRoomScene(roomId: string, scene: string): Promise<void> {
    const rsp = this._http.post<Room>(url(`${roomId}/${scene}`), {});
    firstValueFrom(rsp);
  }

  public async updateGameData<T>(
    roomId: string,
    gameId: string,
    data: T
  ): Promise<void> {
    const rsp = this._http.post<void>(url(`${roomId}/games/${gameId}`), data);
    return firstValueFrom(rsp);
  }
}
