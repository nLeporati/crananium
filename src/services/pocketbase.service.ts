import { Room } from 'src/models/room.model';
import { Injectable } from '@angular/core';
import PocketBase, { RecordSubscription } from 'pocketbase';
import { User, UserStatus } from 'src/models/user.model';
import { Game, GameData } from 'src/models/game.model';

@Injectable({
  providedIn: 'root',
})
export class PocketbaseService {
  private _pb = new PocketBase('http://127.0.0.1:8090');

  public async createUser(username: string): Promise<User> {
    return await this._pb
      .collection('players')
      .create<User>({ name: username, status: 'waiting' });
  }

  public async createRoom(owner: User): Promise<Room> {
    return await this._pb.collection('rooms').create<Room>({
      users: [owner],
      allReady: false,
      scene: 'lobby',
      game: { name: 'songguess' },
    });
  }

  public async getRoom(id: string): Promise<Room> {
    const room = await this._pb
      .collection('rooms')
      .getOne<Room>(id, { expand: 'users' });
    return {
      id: room.id,
      game: room.game,
      users: room.users,
      scene: room.scene,
      allUserStatus: 'idle',
    };
  }

  public async joinRoom(player: User, roomId: string): Promise<Room> {
    const room = await this._pb.collection('rooms').getOne<Room>(roomId);
    return await this._pb
      .collection('rooms')
      .update<Room>(roomId, { ...room, users: [...room.users, player] });
  }

  public async updatePlayer(
    userId: string,
    roomId: string,
    status: UserStatus
  ) {
    const room = await this.getRoom(roomId);

    const userIndex = room.users.findIndex((user) => user.id === userId);
    room.users[userIndex].status = status;

    await this._pb
      .collection('rooms')
      .update<Room>(roomId, { ...room, users: room.users });
  }

  public room(id: string, callback: (data: RecordSubscription<Room>) => void) {
    return this._pb.collection('rooms').subscribe<Room>(id, callback);
  }

  public game(id: string, callback: (data: Game<GameData>) => void) {
    return this._pb
      .collection('games')
      .subscribe<Game<GameData>>(id, (data) => callback(data.record));
  }

  public async updateRoomScene(roomId: string, scene: string): Promise<void> {
    await this._pb.collection('rooms').update<Room>(roomId, { scene });
  }

  public async createGame<GameData>(
    roomId: string,
    gameId: string,
    data: GameData
  ): Promise<Game<GameData>> {
    const game = await this._pb
      .collection('games')
      .create<Game<GameData>>({ name: gameId, data });
    await this._pb
      .collection('rooms')
      .update<Room>(roomId, { game, scene: `game:${gameId}:start` });
    return game;
  }

  public async updateGameData<T>(
    roomId: string,
    gameId: string,
    data: T
  ): Promise<Game<T>> {
    const game = await this._pb
      .collection('games')
      .update<Game<T>>(gameId, { data });
    await this._pb.collection('rooms').update(roomId, { game });
    return game;
  }
}
