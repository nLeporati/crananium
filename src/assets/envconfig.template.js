(function(window) {
  window.envconfig = window.envconfig || {};

  // Environment variables
  window["envconfig"]["apiurl"] = "${API_URL}";
  window["envconfig"]["napsterapikey"] = "${NAPSTER_API_KEY}"
})(this);
