import { KeyMap } from 'src/models';
import { GameConfig } from 'src/models/game.model';

export const DEFAULT_GAME_TIME = 30;

export const GAME_LIST: KeyMap<boolean> = {
  'songguess:chosen': true,
  'songguess:random': true,
  'trivia:random': true,
};

export const DEFAULT_CONFIG: GameConfig = {
  changeDriver: true,
  rounds: 5,
  games: ['trivia:random', 'songguess:chosen', 'songguess:random'],
};

export const GAMES: KeyMap<string> = {
  songguess: 'Song Guess',
  trivia: 'Trivia',
};

export const CATEGORIES: KeyMap<string> = {
  'act-hum': 'Act & Hum',
  'word-play': 'Word Play',
  'sleuth-solve': 'Sleuth & Solve',
  'sketch-sculpt': 'Sketch & Sculpt',
};
