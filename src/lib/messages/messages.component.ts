import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagesStore } from 'src/stores/messages.store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-messages',
  standalone: true,
  imports: [CommonModule],
  providers: [MessagesStore],
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
})
export class MessagesComponent implements OnDestroy {
  messages$?: Observable<string[]>;

  constructor(private readonly _messagesStore: MessagesStore) {
    this.getMessages();
  }

  ngOnDestroy(): void {
    this._messagesStore.disconnect();
  }

  private async getMessages() {
    this.messages$ = await this._messagesStore.getMessages();
  }
}
