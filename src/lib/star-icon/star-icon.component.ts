import { Component } from '@angular/core';

@Component({
  selector: 'app-star-icon',
  standalone: true,
  templateUrl: './star-icon.component.svg',
  styleUrls: ['./star-icon.component.scss'],
})
export class StarIconComponent {}
