import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import multiavatar from '@multiavatar/multiavatar';

@Component({
  selector: 'app-avatar',
  standalone: true,
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
})
export class AvatarComponent implements OnChanges {
  avatar?: SafeHtml;

  @Input() name?: string;

  constructor(private _sanitizer: DomSanitizer) {}

  ngOnChanges(changes: SimpleChanges): void {
    const name = changes['name']?.currentValue as string | undefined;
    this.avatar = name
      ? this._sanitizer.bypassSecurityTrustHtml(multiavatar(name))
      : undefined;
  }
}
