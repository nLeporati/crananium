import { CommonModule } from '@angular/common';
import { Component, Output, EventEmitter, Input, OnInit } from '@angular/core';
import {
  finalize,
  Observable,
  shareReplay,
  take,
  timer,
  ReplaySubject,
  filter,
  takeUntil,
  tap,
} from 'rxjs';
import { DEFAULT_GAME_TIME } from 'src/environments/constants';
import { GameStore } from 'src/stores/game.store';
import { TimeState } from 'src/stores/time.state';
import { AutoDestroy } from 'src/utils';

@Component({
  selector: 'app-timer',
  standalone: true,
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss'],
  imports: [CommonModule],
})
export class TimerComponent implements OnInit {
  timer$?: Observable<number>;

  @Input() time = DEFAULT_GAME_TIME;
  @Output() started = new EventEmitter();
  @Output() finish = new EventEmitter();

  @AutoDestroy private _destroyed$ = new ReplaySubject<void>(1);

  constructor(
    private readonly _gameStore: GameStore,
    private readonly _timeState: TimeState
  ) {}

  ngOnInit(): void {
    this._gameStore
      .getRoom()
      .pipe(
        filter((room) => !this.timer$ && room?.allUserStatus === 'playing'),
        tap(() => {
          this.checkTime();
          this._destroyed$.next();
        }),
        takeUntil(this._destroyed$)
      )
      .subscribe();
  }

  private checkTime() {
    this.time = this._timeState.getDiff(this.time);

    if (this.time > 0) this.startTime();
    else this.finishTime();
  }

  private startTime() {
    this.timer$ = timer(0, 1000).pipe(
      take(this.time),
      finalize(() => this.finishTime()),
      shareReplay(1)
    );

    this._timeState.start(this.time);
    this.started.emit();
  }

  private finishTime() {
    this._timeState.unset();
    this.finish.emit();
  }
}
