import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs';
import { GameState, GameStore } from 'src/stores/game.store';
import { AvatarComponent } from '../avatar/avatar.component';
import { StarIconComponent } from '../star-icon/star-icon.component';

@Component({
  selector: 'app-game-result',
  standalone: true,
  imports: [CommonModule, AvatarComponent, StarIconComponent],
  templateUrl: './game-result.component.html',
  styleUrls: ['./game-result.component.scss'],
})
export class GameResultComponent {
  game$: Observable<GameState | undefined>;

  constructor(private readonly _gameStore: GameStore) {
    this.game$ = this._gameStore.getGame();
  }
}
