import { NgModule } from '@angular/core';

import { FeatherModule } from 'angular-feather';
import { Volume1, Volume2, Settings, VolumeX } from 'angular-feather/icons';

const icons = {
  Volume1,
  Volume2,
  VolumeX,
  Settings,
};

@NgModule({
  imports: [FeatherModule.pick(icons)],
  exports: [FeatherModule],
})
export class IconsModule {}
