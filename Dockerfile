FROM node:16-alpine AS builder
WORKDIR /usr/app
COPY . .
# RUN npm i -g @angular/cli
# RUN npm i -g serve
RUN npm install
RUN npm run build
# EXPOSE 3000
# CMD ["serve", "dist"]

FROM nginx
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=builder /usr/app/dist .
ENTRYPOINT ["nginx", "-g", "daemon off;"]
